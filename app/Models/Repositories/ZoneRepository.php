<?php
namespace App\Models\Repositories;

use App\Models\Zone;
use App\Models\User;

class ZoneRepository extends BaseRepository
{
    public function __construct(Zone $model)
    {
        parent::__construct($model);
    }

    public function getByUser(User $user) {
        return $this->model->where('garden_id', $user->id)->get();
    }
}
